package dominio;


public class ProfesorTitular extends Profesor {

    @Override
    public double calcularValoracion() {

        double sumaValoracion = 0;
        double calcularValoracion = 0;
        for (Merito merito : listaMeritos){
            sumaValoracion += merito.obtenerValoracion();
            calcularValoracion = (sumaValoracion/listaMeritos.size());
        }

        return calcularValoracion;
    }
}


