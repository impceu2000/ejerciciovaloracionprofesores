package dominio;

public class Articulo extends Merito {

    private double indiceDeImpacto;

    public Articulo(String tituloMerito, double indiceDeImpacto) {
        super(tituloMerito);
        this.indiceDeImpacto = indiceDeImpacto;
    }

    @Override
    public double obtenerValoracion() {
        return indiceDeImpacto;
    }
}

