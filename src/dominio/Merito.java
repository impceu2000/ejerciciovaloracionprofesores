package dominio;

public abstract class Merito {

    protected String tituloMerito;

    public Merito(String tituloMerito) {
        this.tituloMerito = tituloMerito;
    }

    public abstract double obtenerValoracion();

}
