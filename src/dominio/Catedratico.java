package dominio;

public class Catedratico extends Profesor {

    @Override
    public double calcularValoracion() {

        double sumaCuadradoValoracion = 0;
        double calcularValoracion = 0;
        for (Merito merito : listaMeritos){
            sumaCuadradoValoracion += Math.pow(merito.obtenerValoracion(),2);
            calcularValoracion = Math.sqrt(sumaCuadradoValoracion/listaMeritos.size());
        }
        return calcularValoracion;
    }
}

