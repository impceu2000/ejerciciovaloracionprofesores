package dominio;

public class Proyecto extends Merito {
    private double financiacionProyecto;

    public Proyecto(String tituloMerito, double financiacionProyecto) {
        super(tituloMerito);
        this.financiacionProyecto = financiacionProyecto;
    }

    @Override
    public double obtenerValoracion() {
        return (financiacionProyecto / 100000);
    }
}
